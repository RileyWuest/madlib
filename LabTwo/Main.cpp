#include <iostream>
#include <cstring>
#include <conio.h>
#include <string>
#include <fstream>

using namespace std;

void PrintToScreen(const string* pResponses)
{
	cout << "One day my " << pResponses[0] << " friend and I decided to go to the " << pResponses[1] << " in " << pResponses[2] << "." << endl;
	cout << "We really wanted to see " << pResponses[3] << " play." << endl;
	cout << "So we " << pResponses[4] << " in the " << pResponses[5] << " and headed down to the " << pResponses[6] << " and bought some " << pResponses[7] << "." << endl;
	cout << "We watched the game and it was " << pResponses[8] << "." << endl;
	cout << "We ate some " << pResponses[9] << " and drank some " << pResponses[10] << "." << endl;
	cout << "We had a " << pResponses[11] << " time, and can't wait to go again." << endl;
}

void PrintToFile()
{
	char yn;
	cout << "Would you like to save output to file? (y/n): ";
	cin >> yn;

	if (yn == 'y')
	{
		string filepath = "";
		ofstream ofs(filepath);
		// run the same function using ofs instead of cout
		ofs.close();
		cout << "Mad lib has been saved to " << filepath << ".\n";
	}
	cout << "Press any key to exit...";
}



int main() {

	const int SIZE = 12;
	string prompts[SIZE];
	string responses[SIZE];
	prompts[0] = "Enter an adjective (describing word): ";
	prompts[1] = "Enter a sport: ";
	prompts[2] = "Enter a city: ";
	prompts[3] = "Enter a person: ";
	prompts[4] = "Enter an action verb (past tense): ";
	prompts[5] = "Enter a vehicle: ";
	prompts[6] = "Enter a place: ";
	prompts[7] = "Enter a noun (thing, plural): ";
	prompts[8] = "Enter an adjective (describing word): ";
	prompts[9] = "Enter a food (plural): ";
	prompts[10] = "Enter a liquid: ";
	prompts[11] = "Enter an adjective (describing word): ";

	for (int i = 0; i < SIZE; i++)
	{
		cout << prompts[i];
		getline(cin, responses[i]);
	}

	PrintToScreen(responses);
	PrintToFile();



	(void)_getch();
	return 0;
}